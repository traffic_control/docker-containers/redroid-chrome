ARG BASE_IMAGE=redroid/redroid:11.0.0-amd64
FROM ${BASE_IMAGE}

ARG CHROME_APK
COPY ${CHROME_APK} /system/app/com.android.chrome.apk