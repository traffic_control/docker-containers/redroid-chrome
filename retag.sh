#!/bin/sh

cat downloader/downloaded/output.json | jq 'to_entries | .[].key' --raw-output | while read -r androidVersion; do
    # echo "android ${androidVersion}"
    cat downloader/downloaded/output.json | jq ".[\"${androidVersion}\"] | to_entries | .[].key" --raw-output | while read -r browserTag; do
        browserVersion=`cat downloader/downloaded/output.json | jq ".[\"${androidVersion}\"][\"${browserTag}\"]" --raw-output`

        if [ $browserTag != $browserVersion ] ; then
            skopeo copy docker://${QUAY_REGISTRY_IMAGE}-${androidVersion}:${browserVersion} docker://${QUAY_REGISTRY_IMAGE}-${androidVersion}:${browserTag}
        fi
    done
done