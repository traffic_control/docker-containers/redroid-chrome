<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;

class DockerRegistry
{
    private $clientId;

    private $clientSecret;

    private $authToken = [];

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client, $clientId = 'php_guzzle_client', $clientSecret = null)
    {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * Lists tags for image
     *
     * @param mixed $imageName 
     *
     * @return array
     */
    public function imageTags($imageName)
    {
        try {
            $response = $this->client->get('/v2/' . $imageName . '/tags/list', $this->addHeaderValues());
        } catch (ClientException $th) {
            if ($th->getCode() == 401) {
                $this->authToken = [];
                $this->getToken($th->getResponse());
                $response = $this->client->get('/v2/' . $imageName . '/tags/list', $this->addHeaderValues());
            }
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getManifest($imageName, $reference = 'latest')
    {
        try {
            $response = $this->client->get('/v2/' . $imageName . '/manifests/' . $reference, $this->addHeaderValues());
            return json_decode($response->getBody()->getContents());
        } catch (ClientException $th) {
            if ($th->getCode() == 401) {
                $this->authToken = [];
                $this->getToken($th->getResponse());
                $response = $this->client->get('/v2/' . $imageName . '/manifests/' . $reference, $this->addHeaderValues());
                return json_decode($response->getBody()->getContents());
            }
            throw $th;
        }
    }

    protected function addHeaderValues($options = []) {
        if ($this->authToken) {
            $options['headers']['Authorization'] = implode(',', $this->authToken);
        }
        return $options;
    }

    protected function getToken(Response $response)
    {
        if ($authenticateHeaders = $response->getHeader('Www-Authenticate')) {
            foreach ($authenticateHeaders as $authenticateHeader ) {
                list(, $authString) = explode(' ', $authenticateHeader);
                $authParameterStrings = explode(',', $authString);
                $authParameters = [];

                foreach ($authParameterStrings as $authParameterString) {
                    list($authParameter, $value) = explode('=', $authParameterString, 2);
                    if (!isset($authParameters[$authParameter])) {
                        $authParameters[$authParameter]=[];
                    }
                    $authParameters[$authParameter][]=trim($value, '"');
                }
                $realm = $authParameters['realm'];
                unset($authParameters['realm']);

                $tokenResponse = $this->client->get(array_pop($realm), [
                    'query' => array_merge(array_map(function ($values) {
                        return implode(', ', $values);
                    }, $authParameters), [
                    'client_id' => $this->clientId,
                ])]);
                $decodedToken = json_decode($tokenResponse->getBody()->getContents(), true);

                $this->authToken[] = 'Bearer ' . $decodedToken['token'];
            }
        }
    }
}
