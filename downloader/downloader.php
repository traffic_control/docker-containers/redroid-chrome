<?php

use Credy\BrowserkitFlaresolverr\FlareSolverr;
use Credy\BrowserkitFlaresolverr\FlareSolverrBrowser;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DomCrawler\Crawler;

require_once 'vendor/autoload.php';
require_once 'src/FlareSolverrV3.php';
require_once 'src/Docker.php';

$quayClient = new DockerRegistry(new Client([
    'base_uri' => 'https://quay.io/',
]));

$flareSolverr = new FlareSolverr('http://flaresolverr:8191');

$flaresolverrSession = $flareSolverr->getSession();

$flareSolverrBrowser = new FlareSolverrBrowser($flaresolverrSession);

$response = $flareSolverrBrowser->request('GET', 'https://www.apkmirror.com/uploads/?appcategory=chrome');

$links = $response->filter('.widget_appmanager_recentpostswidget .appRow .table-row')->each(function (Crawler $node) {
    return $node->filter('a.fontBlack')->link()->getUri();
});

array_map(function ($uri) use ($flareSolverrBrowser, $quayClient, $flaresolverrSession) {
    try {
        $response = $flareSolverrBrowser->request('GET', $uri);

        $versionText = $response->filter('#breadcrumbs .active')->first()->text();

        $androidVersions = ['14.0.0', '13.0.0', '12.0.0', '11.0.0', '10.0.0', '9.0.0', '8.1.0'];
        $exists = true;
        foreach ($androidVersions as $androidVersion) {
            try {
                $quayClient->getManifest(substr(getenv('QUAY_REGISTRY_IMAGE'), strlen(getenv('QUAY_REGISTRY')) + 1) . '-' . $androidVersion, $versionText);
                echo 'Existing ' . $versionText . ' for Android ' . $androidVersion .PHP_EOL;
            } catch (ClientException $th) {
                if ($th->getCode() === 404) {
                    $exists = false;
                    break ;
                }
            }
        }
        if ($exists) {
            return true;
        }

        echo 'Trying to download chrome ' . $versionText . ' APK' . PHP_EOL;

        $apks = array_filter(iterator_to_array($response->filter('.variants-table .table-row.headerFont')), function (DOMElement $domElement) {
            if (strpos($domElement->textContent, 'x86_64') === false) {
                return false;
            }
            $crawler = new Crawler($domElement);
            if (!$crawler->filter('.apkm-badge:not(.success)')->count()) {
                return false;
            }
            return true;
        });
        if (!count($apks)) {
            echo 'No APKs found' . PHP_EOL;
            return;
        }

        $crawler = new Crawler($apks, $uri);

        $downloadUri = $crawler->filter('.accent_color')->link()->getUri();

        $response = $flareSolverrBrowser->request('GET', $downloadUri);

        $downloadFileUri = $response->filter('article a[rel=nofollow].downloadButton')->link()->getUri();

        $downloadPageResponse = $flareSolverrBrowser->request('GET', $downloadFileUri);

        $apkUri = $downloadPageResponse->filter('article a[rel=nofollow]')->link()->getUri();

        echo 'Got APK url' . PHP_EOL;

        $downloadClient = new Client([
            RequestOptions::HEADERS => [
                'User-Agent' => $flaresolverrSession->getUserAgent()
            ]
        ]);

        try {
            $downloadClient->get($apkUri, [
                RequestOptions::SINK => __DIR__ . '/downloaded/' . $versionText . '.apk'
            ]);
        } catch (ClientException | ConnectException $th) {
            unlink(__DIR__ . '/downloaded/' . $versionText . '.apk');

            if ($th instanceof ClientException && $th->getCode() == 429) {
                throw $th; // If code is 429, we are throtteled, no need to go further
            }

            echo $th->getCode() . PHP_EOL;
            echo $th->getMessage() . PHP_EOL;
        }
    } catch (Throwable $th) {
        echo $th->getCode() . PHP_EOL;
        echo $th->getMessage() . PHP_EOL;

        if (glob(__DIR__ . '/downloaded/*.apk')) {
            exit(0);
        }
        exit(1);
    }
}, $links);