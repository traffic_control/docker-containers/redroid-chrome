<?php

use Composer\Semver\Semver;
use GuzzleHttp\Client;

require 'vendor/autoload.php';
require_once 'src/Docker.php';

$dockerClient = new DockerRegistry(new Client([
    'base_uri' => 'https://quay.io/',
]));

$chromeVersions = [];

$androidVersions = ['14.0.0', '13.0.0', '12.0.0', '11.0.0', '10.0.0', '9.0.0', '8.1.0', ];
foreach ($androidVersions as $androidVersion) {
    if (!isset($chromeVersions[$androidVersion])) {
        $chromeVersions[$androidVersion]=[];
    }
    $tagsResponse = $dockerClient->imageTags(substr(getenv('QUAY_REGISTRY_IMAGE'),strlen(getenv('QUAY_REGISTRY'))+1) . '-' . $androidVersion);

    $chromeVersions[$androidVersion] = $tagsResponse['tags'];
}

$chromeVersions = array_map(function ($chromeVersions) {
    $chromeVersions = array_filter($chromeVersions, function ($versionString) {
        return $versionString !== 'latest';
    });
    foreach ($chromeVersions as $chromeVersion) {
        $versionParts = explode('.', $chromeVersion);
        while (count($versionParts) !== 0) {
            $version = implode('.', $versionParts);
            array_push($chromeVersions, $version);
            array_pop($versionParts);
        }
    }
    $chromeVersions = array_unique($chromeVersions);

    $satisfiedChromeVersions = array_combine($chromeVersions, array_map(function ($currentVersion) use ($chromeVersions) {
        return array_filter($chromeVersions, function ($version) use ($currentVersion) {
            return strpos($version, $currentVersion) === 0;
        });

    }, $chromeVersions));

    $newestChromesForTags = array_map(function ($versions) {
        $sortedVersions = Semver::sort($versions);
        return array_pop($sortedVersions);
    }, $satisfiedChromeVersions);

    $sortedVersions = Semver::sort($newestChromesForTags);
    $newestChromesForTags['latest'] = array_pop($sortedVersions);

    return $newestChromesForTags;
}, $chromeVersions);

echo json_encode($chromeVersions);
