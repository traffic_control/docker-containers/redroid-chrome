#!/bin/sh

for file in downloader/downloaded/*.apk ; do
    tag=${file:22:-4}

    docker build \
    --pull \
    --build-arg=BASE_IMAGE=redroid/redroid:${ANDROID_VERSION}-latest \
    --build-arg=CHROME_APK=${file} \
    --tag=${QUAY_REGISTRY_IMAGE}-${ANDROID_VERSION}:${tag} \
    ./

    docker push ${QUAY_REGISTRY_IMAGE}-${ANDROID_VERSION}:${tag}

    echo ${tag}
done